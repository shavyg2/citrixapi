﻿namespace Citrix.Authentication
{
    public interface IAuthenticationMethod
    {
    }

    public abstract class AuthenticationMethod:IAuthenticationMethod
    {
        public virtual string Name
        {
            get { return this.GetType().Name; }
        }

        public virtual string Path { get; set; }

        protected AuthenticationMethod(string url)
        {
            this.Path = url;
        }

        public override string ToString()
        {
            return string.Format("{0}:{1}", this.Name, this.Path);
        }

        public static AuthenticationMethod CreateFromMethodName(string methodname,string url)
        {
            AuthenticationMethod method;
            switch (methodname)
            {
                case "IntegratedWindows":
                    method = new IntegratedWindows(url);
                    break;
                case "ExplicitForms":
                    method = new ExplicitForms(url);
                    break;
                case "PostCredentials":
                    method= new PostCredentials(url);
                    break;
                case "CitrixAuth":
                    method = new CitrixAuth(url);
                    break;
                case "CitrixAGBasic":
                    method = new CitrixAGBasic(url);
                    break;
                default:
                    method = null;
                    break;

            }

            return method;
        }
    }

    public class IntegratedWindows:AuthenticationMethod
    {
        public IntegratedWindows(string url) : base(url)
        {
            //this.Name = "IntegratedWindows";
        }
    }

    public class ExplicitForms : AuthenticationMethod
    {
        public ExplicitForms(string url) : base(url)
        {
            //this.Name = "ExplicitForms";
        }
    }

    public class PostCredentials : AuthenticationMethod
    {
        public PostCredentials(string url) : base(url)
        {
            //this.Name = "PostCredentials";
        }
    }

    public class CitrixAuth : AuthenticationMethod
    {
        public CitrixAuth(string url)
            : base(url)
        {
            //this.Name = "CitrixAuth";
        }
    }

    public class CitrixAGBasic : AuthenticationMethod
    {
        public CitrixAGBasic(string url)
            : base(url)
        {
            //this.Name = "CitrixAuth";
        }
    }
}
