﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Citrix
{
    public class CitrixResource
    {
        public string Description { get; set; }
        public string IconUrl { get; set; }
        public string Id { get; set; }
        public string LaunchStatusUrl { get; set; }
        public string LaunchUrl { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string ShortcutValidationUrl { get; set; }
        public string SubscriptionUrl { get; set; }
        public string PowerOffUrl { get; set; }
        public string DesktopAssignmentType { get; set; }
        public bool IsDesktop { get; set; }
        public string[] ClientTypes { get; set; }


        public CitrixResource(JObject d)
        {
            DesktopAssignmentType = d.Property("desktopassignmenttype") != null ? d.Property("desktopassignmenttype").Value.ToString() : "";
            IconUrl = d.Property("iconurl") != null ? d.Property("iconurl").Value.ToString() : "";
            Id = d.Property("id") != null ? d.Property("id").Value.ToString() : "";
            
            IsDesktop = d.Property("isdesktop") != null ? (bool)d.Property("isdesktop").Value : false;
            
            LaunchStatusUrl = d.Property("launchstatusurl") != null ? d.Property("launchstatusurl").Value.ToString() : "";
            LaunchUrl = d.Property("launchurl") != null ? d.Property("launchurl").Value.ToString() : "";
            Name = d.Property("name") != null ? d.Property("name").Value.ToString() : "";
            Path = d.Property("path") != null ? d.Property("path").Value.ToString() : "";
            PowerOffUrl = d.Property("poweroffurl") != null ? d.Property("poweroffurl").Value.ToString() : "";
            ShortcutValidationUrl = d.Property("shortcutvalidationurl") != null ? d.Property("shortcutvalidationurl").Value.ToString() : "";
            SubscriptionUrl = d.Property("subscriptionurl") != null ? d.Property("subscriptionurl").Value.ToString() : "";


            
            IList<String> _ClientTypes = new List<string>();
            int i = 0;
            foreach (string type in d.Property("clienttypes").Value)
            {
                _ClientTypes.Add(type);// = type;
            }

            ClientTypes = _ClientTypes.ToArray();
        }

        public override string ToString()
        {
            var Properties = this.GetType().GetProperties();
            var sb = new StringBuilder();
            for (int i = 0; i < Properties.Length; i++)
            {
                string propName = Properties[i].Name;
                object propValue = Properties[i].GetValue(this) ?? "";

                string propValueDisplay;
                if (propValue.GetType().IsArray)
                {
                    propValueDisplay = String.Join(", ", (string[])propValue);
                }
                else if(propValue.GetType().Name =="Boolean")
                {
                    propValueDisplay = (bool) propValue ? "true" : "false";
                }
                else
                {
                    propValueDisplay = (string)propValue;
                }
                sb.Append(string.Format("{0}:{1}{2}", propName, propValueDisplay, Environment.NewLine));
            }
            return sb.ToString();
        }
    }
}
