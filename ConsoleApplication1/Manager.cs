﻿using System;
using System.Collections.Generic;
//using System.Dynamic;
using System.IO;
//using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
//using System.Web.Script.Serialization;
using System.Xml;
using Citrix.Authentication;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
//using System.Security.Cryptography.X509Certificates;

namespace Citrix
{

    public delegate void NetScalarGateWayProtetected(HttpWebResponse response);


    /// <summary>
    /// The Manager Class to Manager Access to Citrix Environment. 
    /// This Provides Access to Supported Authentication Methods and many other tool for navigating Citrix Web Interface.
    /// </summary>
    public class Manager
    {

        #region Fields
        private string _default_path;
        private Dictionary<string, string> ExtraHeader;
        private int retries;
        #endregion

        public event OnTimeOut ontimeout;

        #region Properties
        private String default_path
        {
            get { return _default_path; }
            set
            {
                if (value == null)
                {
                    value = "";
                }
                if (value.Length > 0 && value[value.Length - 1] != '/')
                {
                    value += "/";
                }
                _default_path = value;
            }
        }

        private Uri Server { get; set; }

        protected const string HomeConfigurationPath = "Home/Configuration";
        protected const string GetUserName = "Authentication/GetUserName";
        protected const string ResourceList = "Resources/List";
        protected const string GetAuthentication = "Authentication/GetAuthMethods";
        protected const string ExplicitFormsLogin = "ExplicitAuth/Login";
        protected const string ExplicitFormsLoginAttempt = "ExplicitAuth/LoginAttempt";
        protected const string CitrixAGBasicLogin = "GatewayAuth/Login";



        private bool HTTPS
        {
            get { return Server.ToString().ToLower().IndexOf("https") > -1; }
        }


        private CookieContainer _cookieContainer;

        public CookieContainer cookieContainer
        {
            get { return _cookieContainer; }
        }

        #endregion

        public event NetScalarGateWayProtetected onNetScalarProtected;

        #region Constructors
        public Manager(Uri server, string default_path = null)
        {
            this.Server = server;
            this.default_path = default_path;
            ResetCookies();
            ResetHeaders();
            System.Net.ServicePointManager.Expect100Continue = false;
            System.Net.ServicePointManager.CertificatePolicy = new MyPolicy();
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
        }

        public Manager(string server, string default_path = null)
            : this(new Uri(server), default_path)
        {
        }
        #endregion


        #region ResetMethods
        /// <summary>
        /// Resets the CookieContainer Used.
        /// </summary>
        public virtual void ResetCookies()
        {
            _cookieContainer = new CookieContainer();
        }


        /// <summary>
        /// Reset the default Headers that are used for each Request. This is used in the BaseRequest.
        /// <see cref="Citrix.Manager.BaseRequest(Uri,string)"/>
        /// </summary>
        public virtual void ResetHeaders()
        {
            ExtraHeader = new Dictionary<string, string>();
        }
        #endregion


        /// <summary>
        /// Gets a list of supported authentication method available on the Citrix Server
        /// </summary>
        /// <returns>A list of All the Supported Authentication Methods.</returns>
        public IList<AuthenticationMethod> GetAuthenticationMethod()
        {
            var RequestUri = new Uri(new Uri(this.Server, default_path), ResourceList);

            #region Create Web Request
            HttpWebRequest request = BasePostRequest(RequestUri);
            request.Timeout = 5000;
            request.ContentLength = 0;
            #endregion


            #region Process Web Response
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
                if (response.Headers["Location"] != null)
                {
                    response.Close();
                    if (onNetScalarProtected != null)
                    {
                        onNetScalarProtected.Invoke(response);
                        return null;
                    }
                }
            }
            catch (WebException error)
            {
                if (error.Status.ToString() == "Timeout" && retries <= 5)
                {
                    if (ontimeout != null)
                    {
                        ontimeout.Invoke(error);
                    }
                }
                else if (error.Response == null)
                {
                    throw error;
                }
                else
                {
                    response = (HttpWebResponse)error.Response;

                }
            }
            processResponse(response);

            //Console.WriteLine(response.Headers);

            /*
             Second Request this time with the correct Authentication Cookies
             */


            RequestUri = new Uri(new Uri(this.Server, default_path), GetAuthentication);
            request = BasePostRequest(RequestUri);
            request.ContentLength = 0;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException error)
            {
                if (error.Response == null)
                {
                    throw error;
                }
                else
                {
                    response = (HttpWebResponse)error.Response;
                }
            }
            processResponse(response);

            string responseText = new StreamReader(response.GetResponseStream()).ReadToEnd();

            XmlReader reader = XmlReader.Create(new StringReader(responseText));

            IList<AuthenticationMethod> methods = new List<AuthenticationMethod>();

            using (reader)
            {
                while (reader.Read())
                {
                    IAuthenticationMethod method;
                    bool isMethod = false;
                    string methodname = null;
                    string url = null;

                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (reader.Name == "method")
                            {
                                isMethod = true;
                                reader.MoveToNextAttribute();
                                methodname = reader.Value;
                                reader.MoveToNextAttribute();
                                url = reader.Value;
                                var methodInstance = AuthenticationMethod.CreateFromMethodName(methodname, url);
                                methods.Add(methodInstance);
                            }
                            break;
                    }
                }
            }

            #endregion
            return methods;
        }

        /// <summary>
        /// Process the response from a web request and calls other methods that needs to process a response.
        /// </summary>
        /// <param name="res"></param>
        protected virtual void processResponse(HttpWebResponse res)
        {
            processCsrfToken(res);
            //processSessionID(res);
        }


        /// <summary>
        /// Process a response <see cref="Citrix.Manager.processResponse"/>
        /// </summary>
        /// <param name="res"></param>
        protected virtual void processCsrfToken(HttpWebResponse res)
        {
            if (res != null && res.Cookies != null)
            {
                foreach (Cookie cookie in res.Cookies)
                {
                    if (cookie.Name == "CsrfToken")
                    {
                        ExtraHeader["Csrf-Token"] = cookie.Value;
                    }
                }
            }
        }

        protected virtual void processSessionID(HttpWebResponse res)
        {
            foreach (Cookie cookie in res.Cookies)
            {
                if (cookie.Name == "ASP.NET_SessionId")
                {
                    ExtraHeader["ASP.NET_SessionId"] = cookie.Value;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns>A list of all the supported Authentication Methods that this API supports.</returns>
        public static IList<Type> GetSupportedAuthentications()
        {
            IList<Type> supported = new List<Type>();
            supported.Add(Type.GetType("Citrix.Authentication.ExplicitForms"));
            supported.Add(Type.GetType("Citrix.Authentication.CitrixAGBasic"));
            return supported;
        }

        #region BaseRequestMethods

        /// <summary>
        /// This is used to get the base template for all request. This method can be overriden if needed to change the default behaviour of the base request.
        /// </summary>
        /// <param name="path">The path that is being requested.</param>
        /// <param name="method">The Method type to use in the request.</param>
        /// <returns>A Template HttpWebRequest</returns>
        protected virtual HttpWebRequest BaseRequest(Uri path, string method = "POST")
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(path);
            request.Method = method ?? "POST";
            request.CookieContainer = cookieContainer;
            request.ContentType = "application/x-www-form-urlencoded";

            request.Headers.Set("X-Citrix-IsUsingHTTPS", HTTPS ? "Yes" : "No");
            //request.UserAgent = "Base User Agent";

            request.KeepAlive = true;
            request.AllowAutoRedirect = false;

            foreach (string key in ExtraHeader.Keys)
            {
                request.Headers.Set(key, ExtraHeader[key]);
            }
            return request;
        }

        private HttpWebRequest BaseRequest(string path, string method = null)
        {
            return BaseRequest(new Uri(path), method);
        }

        private HttpWebRequest BasePostRequest(string path, string method = null)
        {
            return BaseRequest(path, method);
        }

        private HttpWebRequest BaseGetRequest(string path, string method = null)
        {
            return BaseRequest(path, method);
        }

        private HttpWebRequest BasePostRequest(Uri path, string method = null)
        {
            return BaseRequest(path, method);
        }

        private HttpWebRequest BaseGetRequest(Uri path, string method = null)
        {
            return BaseRequest(path, method);
        }
        #endregion

        public static bool isSupported(Type t)
        {
            return GetSupportedAuthentications().Contains(t);
        }

        public static bool isSupported(AuthenticationMethod method)
        {
            return isSupported(method.GetType());
        }

        public virtual bool Authenticate(AuthenticationMethod method, string username, string password)
        {
            switch (method.Name)
            {
                case "CitrixAGBasic":
                    return CitrixAGBasicAuthentication();
                case "ExplicitForms":
                    return ExplicitFormAuthentication(username, password);
                default:
                    return false;
            }
        }

        private bool ExplicitFormAuthentication(string username, string password)
        {
            Uri requestUri;
            HttpWebRequest request;
            HttpWebResponse response;
            string responseText = null;
            requestUri = new Uri(new Uri(Server, default_path), ExplicitFormsLogin);

            request = BaseRequest(requestUri);
            request.KeepAlive = true;
            request.ContentLength = 0;

            try
            {
                response = (HttpWebResponse)request.GetResponse();
                processResponse(response);
                var responseStream = response.GetResponseStream();
                responseText = new StreamReader(responseStream).ReadToEnd();
                responseStream.Close();
            }
            catch (WebException e)
            {
                throw e;
            }

            responseText = null;


            requestUri = new Uri(new Uri(Server, default_path), ExplicitFormsLoginAttempt);
            request = BaseRequest(requestUri);
            request.KeepAlive = true;
            string data = string.Format("username={0}&password={1}&saveCredentials=false&loginBtn=Log+On&StateContext=", Uri.EscapeDataString(username), Uri.EscapeDataString(password));
            byte[] bdata = Encoding.UTF8.GetBytes(data);
            request.ContentLength = bdata.Length;

            var dataStream = request.GetRequestStream();
            dataStream.Write(bdata, 0, bdata.Length);
            try
            {
                response = (HttpWebResponse)request.GetResponse();
                processResponse(response);
                responseText = new StreamReader(response.GetResponseStream()).ReadToEnd();
            }
            catch (WebException e)
            {
                throw e;
            }

            if (responseText != null)
            {
                XmlReader reader;

                reader = XmlReader.Create(new StringReader(responseText));
                while (reader.Read())
                {
                    if (reader.Name == "Result")
                    {
                        string content = reader.ReadString();
                        if (content.ToLower() == "Success".ToLower())
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            response.Close();
            return false;
        }

        private bool CitrixAGBasicAuthentication()
        {
            Uri requestUri;
            HttpWebRequest request;
            HttpWebResponse response;
            string responseText = null;
            requestUri = new Uri(new Uri(Server, default_path), CitrixAGBasicLogin);
            request = BaseRequest(requestUri);
            request.ContentLength = 0;
            request.KeepAlive = true;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
                processResponse(response);
                return ValidateLogin(response);
            }
            catch (WebException e)
            {
                throw e;
            }
            return false;
        }

        public bool ValidateLogin(HttpWebResponse response)
        {
            var responseText = new StreamReader(response.GetResponseStream()).ReadToEnd();
            if (responseText != null)
            {
                XmlReader reader;

                reader = XmlReader.Create(new StringReader(responseText));
                while (reader.Read())
                {
                    if (reader.Name == "Result")
                    {
                        string content = reader.ReadString();
                        if (content.ToLower() == "Success".ToLower())
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }

            return false;
        }

        public bool isAuthenticated()
        {
            Uri requestUri = new Uri(new Uri(Server, default_path), GetUserName);

            HttpWebRequest request = this.BasePostRequest(requestUri);
            request.ContentLength = 0;

            try
            {
                request.GetResponse();
                return true;
            }
            catch (WebException e)
            {
                return false;
            }
        }

        public Uri ConcatUri(string uri)
        {
            if (uri.Length > 0 && uri[0] == '/')
            {
                uri = uri.Substring(1);
            }
            return new Uri(new Uri(Server, default_path), uri);
        }

        public IList<CitrixResource> GetResources()
        {
            Uri requestUri = new Uri(new Uri(Server, default_path), ResourceList);
            HttpWebRequest request = this.BasePostRequest(requestUri);
            request.ContentLength = 0;
            HttpWebResponse response;
            string responseText;
            IList<CitrixResource> resources = new List<CitrixResource>();

            try
            {
                response = (HttpWebResponse)request.GetResponse();
                var responseStream = response.GetResponseStream();
                responseText = (new StreamReader(responseStream).ReadToEnd());
                responseStream.Close();
                response.Close();

                JObject result = (JObject)JsonConvert.DeserializeObject(responseText);
                foreach (JObject resource in result["resources"])
                {
                    var cr = new CitrixResource(resource);
                    resources.Add(cr);
                }
            }
            catch (WebException e)
            {

            }

            return resources;

        }


        public void ForceNetScalar(string location, string username, string password)
        {
            Uri requestUri = new Uri(Server, location);
            var request = BasePostRequest(requestUri);
            request.AllowAutoRedirect = true;
            var requestData = ConvertData(
                    new KeyValuePair<string, string>("login", username),
                    new KeyValuePair<string, string>("passwd", password)
                );
            request.Referer = new Uri(Server, "/vpn/index.html").ToString();
            request.ContentLength = requestData.Length;
            request.GetRequestStream().Write(requestData, 0, requestData.Length);
            request.GetResponse().Close();
        }

        public byte[] ConvertData(params KeyValuePair<string, string>[] data)
        {
            IList<String> datatosend = new List<string>();

            foreach (KeyValuePair<string, string> d in data)
            {
                datatosend.Add(string.Format("{0}={1}", Uri.EscapeDataString(d.Key), Uri.EscapeDataString(d.Value)));
            }
            return Encoding.UTF8.GetBytes(String.Join("&", datatosend));
        }
    }
}
