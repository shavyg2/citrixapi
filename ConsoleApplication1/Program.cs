﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Citrix.Authentication;


namespace Citrix
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string username = "loribonari";
            string password = "Montfort20141";
            var manager=new Manager("https://acces.montfort.on.ca/","Citrix/hmWeb");


            manager.ontimeout += delegate(WebException error)
            {
                Console.WriteLine(error);
            };
            
            //string username = "panacea\\brian";
            //string password = "Batman123";
            //var manager = new Manager("http://10.20.0.242/", "Citrix/mypanaceaWeb/"); 
            


            //Methods available on the Server for Authentication
            IList<AuthenticationMethod> methods =null,temp =null;


            #region NetScalar Authentication
            manager.onNetScalarProtected += delegate(HttpWebResponse response)
            {
                //Citrix is protected using NetScalar Gateway. This isn't an issue however

                /*
                 * This looks a little confusing. But when my GetAuthentication method fails
                 * because it is protected from NetScalar i need to redo getting those 
                 * authentication methods.
                 */
                
                /*
                 * Here i force NetScalar to Log me In
                 */
                manager.ForceNetScalar("/cgi/login",username,password);

                /*
                 * Once I have the cookies i need from netscalar I can now proceed with getting the authentication methods
                 * I have to make sure that i receive the available authentication methods 
                 * before i assign it to the method.
                 */
                temp= manager.GetAuthenticationMethod();
                if (temp != null)
                {
                    methods = temp;
                }
                
            };
            #endregion


            temp = manager.GetAuthenticationMethod();
            if (temp != null)
            {
                methods = temp;
            }


            if (methods == null)
            {
                /*
                 * Something is really wrong.
                 * I can't get Any Authentication method from the citrix server.
                 * You would have to investigate at this point why you can't get authentication methods available.
                 * it's Not Network Connection that would throw an exception.
                 * For some reason you are connectin with status 200 ok
                 * but there are not available methods.
                 * Might be incorrect URL at this point.
                 * 
                 * Use a Network Sniff to see what is being send and receieve.
                 */
                Console.WriteLine("Some Error has occured. Can't Pass NetScalar Gateway or Can't get Authentication Methods\nExiting Application.");
                Console.ReadLine();
                Environment.Exit(1);
            }

            AuthenticationMethod method=null;

            //todo
            /*
             * I need to set up some kind of preference as to which gateway to use.
             * This is because if i use netscalar it is easier to use CitrixAGBasic because this require no
             * configuration and it just works.
             * 
             * However if there is no Gateway and i do need to Authenticate then the easiest way to authenticate is 
             * Explicit Form. 
             * 
             * Right now those are the only two Authentication Methods that are supported by this API
             */

            Console.WriteLine("Supported Authentication Methods:");
            foreach (AuthenticationMethod am in methods)
            {
                Console.WriteLine("{0}",am.Name);
            }

            foreach (var authenticationMethod in methods.Reverse())
            {
            
                //Checks is this api supports the authication method
                if (Manager.isSupported(authenticationMethod))
                {
                    Console.WriteLine("Using {0} to Authenticate",authenticationMethod.Name);
                    method = authenticationMethod;
                    break;
                }
            }

            if (method != null)
            {
                //supported
                if (manager.Authenticate(method, username, password))
                {
                    Console.WriteLine("Successfully Authenticated");

                    Console.WriteLine("Check Authentication");
                    if (manager.isAuthenticated())
                    {
                        Console.WriteLine("Yup, it's Authenticated");

                        var resources = manager.GetResources();
                        foreach (CitrixResource resource in resources)
                        {
                            if (resource.IsDesktop)
                            {
                                Console.WriteLine("Citrix Desktop Resource Found");
                                Console.WriteLine("Name:{0}\nLaunch URL:{1}", resource.Name, manager.ConcatUri(resource.LaunchUrl));
                            }
                        }
                    }
                }
                else
                {
                    /*
                     * When using the NetScalar Gateway to Authenticate it causes some problems.
                     * There is a Domain that we need to get to 
                     */
                    Console.WriteLine("Incorrect Credentials");
                }

                
            }
            else
            {
                //No support
                String[] authMethods = new string[methods.Count];
                for (int i = 0; i < methods.Count; i++)
                {
                    authMethods[i] = methods[i].Name;
                }
                Console.WriteLine("This API doens't support the following Authentication Methods{0}{1}",Environment.NewLine,String.Join(", ",authMethods));
            }

            Console.ReadLine();
        }
    }
}
